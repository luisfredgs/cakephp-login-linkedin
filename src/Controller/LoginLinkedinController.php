<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Security;
use Cake\Network\Http\Client;

class LoginLinkedinController extends AppController{

    public $autoRender = false;

    private $clientID = '774sd33ae00u11';

    private $clientSecret = 'niY0lcq9EysTNCde';

    private $callbackURL = 'http://luisfred.vid/cakephp/login_linkedin/callback';

    private $scope = 'r_basicprofile r_emailaddress w_share';

    private $state = 'DCEeFWf45A53sdfKef424';

    private $grantType = 'authorization_code';

    private $responseType = 'code';

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['auth', 'callback']);
    }

    public function auth()
    {
        $params = [
            'response_type' => $this->responseType,
            'client_id' => $this->clientID,
            'scope' => $this->scope,
            'state' => $this->state,
            'redirect_uri' => $this->callbackURL,
        ];

        $this->redirect('https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params));
    }


    public function callback()
    {
        if( !empty( $this->request->query['code'] ) && !empty( $this->request->query['state'] ) && ( $this->request->query['state'] == $this->state  ) )
        {
            $params = [
                'grant_type' => $this->grantType,
                'client_id' => $this->clientID,
                'client_secret' => $this->clientSecret,
                'code' => $this->request->query['code'],
                'redirect_uri' => $this->callbackURL,
            ];

            $http = new Client();
            $response = $http->get('https://www.linkedin.com/uas/oauth2/accessToken', $params);
            $token = json_decode($response->body);

            $user_linkedin = $this->_fetchLinkedin('/v1/people/~:(firstName,lastName,emailAddress)', $token->access_token);

            if( !empty( $user_linkedin->emailAddress ) )
            {
                $this->loadModel('Users');
                $password = Security::hash($user_linkedin->emailAddress, 'sha1', true);
                $user = $this->Users->findByEmail($user_linkedin->emailAddress)->first();
                if( empty( $user ) )
                {
                    $data = [
                        'email' => $user_linkedin->emailAddress,
                        'password' => $password,
                        'fname' => $user_linkedin->firstName,
                        'lname' => $user_linkedin->lastName
                    ];
                    $user = $this->Users->newEntity($data);
                    $this->Users->save($user);
                    unset($data['password']);
                    $this->Auth->setUser($data);
                    $this->request->session()->delete("Auth.User.password");
                    $this->redirect($this->Auth->redirectUrl());
                }else{
                    $user = $user->toArray();
                    $data = ['id' => $user['id'], 'email' => $user['email'], 'fname' => $user['fname']];
                    $this->Auth->setUser($data);
                    $this->request->session()->delete("Auth.User.password");
                    $this->redirect($this->Auth->redirectUrl());
                }
            }
        }else
            if( !empty( $this->request->query['error'] ) && $this->request->query['error'] == 'access_denied' ){
                $error_message = $this->request->query['error_description'];
                $this->Flash->error($error_message, ['key' => 'auth']);
                $this->redirect($this->Auth->logout());
            }else if ( $this->request->query['state'] != $this->state ){
                $error_message = "Security Error";
                $this->Flash->error($error_message, ['key' => 'auth']);
                $this->redirect($this->Auth->logout());
            }
    }

    protected function _fetchLinkedin($resource, $access_token = '') {
        $params = [
            'oauth2_access_token' => $access_token,
            'format' => 'json',
        ];
        $http = new Client();
        $response = $http->get('https://api.linkedin.com' . $resource, $params);

        return json_decode($response->body);
    }




}

?>